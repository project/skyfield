<?php
/**
 * @file
 * Main module file, only hooks are allowed here.
 */

/**
 * Requirements.
 */
require_once 'model/sky_field.db.inc';
require_once 'includes/sky_field.field.inc';
require_once 'includes/sky_field.api.inc';

// Constants.
define('SKY_FIELD_OPTIONS_FIELD_DESCRIPTION', 'Possible values this field can contain. Enter one value per line in the key|label format.');
define('SKY_FIELD_MENU_WEIGHT', 50);
define('SKY_FIELD_ACCESS_DENY', 'deny');
define('SKY_FIELD_ACCESS_ALLOW', 'allow');

/**
 * Implements hook_permission().
 */
function sky_field_permission() {
  $items = array();
  $items['admin sky fields'] = array(
    'title' => t('Administer sky fields'),
    'description' => t('Access to sky fields settings form'),
  );
  $items['manage sky fields'] = array(
    'title' => t('Manage sky fields'),
    'description' => t('Create, update, delete sky fields. Access to sky fields also dependends on access to entity. For example, user needs access to node update to add sky fields to node.'),
  );
  $items['view sky fields'] = array(
    'title' => t('View sky fields'),
  );
  return $items;
}

/**
 * Implements hook_menu().
 */
function sky_field_menu() {
  $items = array();
  $items['admin/config/content/sky-field'] = array(
    'title' => 'Sky field settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sky_field_admin_settings_form'),
    'access arguments' => array('admin sky fields'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/pages/sky_field.admin.inc',
  );
  $entity_menu_base = sky_field_get_entity_menu_base();
  foreach ($entity_menu_base as $type => $base) {
    if (!sky_field_is_sky_field_entity_type($type)) {
      continue;
    }
    $field_object_shift = count(explode('/', $base)) - 1;
    $items[$base . '/%sky_field_menu_item/sky-field'] = array(
      'title' => 'Sky fields',
      'load arguments' => array($type, $field_object_shift + 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        'sky_field_sky_field_form',
        $type,
        $field_object_shift + 1,
      ),
      'access callback' => 'sky_field_entity_access',
      'access arguments' => array($type, $field_object_shift + 1, 'update'),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'includes/sky_field.form.sky_field.inc',
      'weight' => SKY_FIELD_MENU_WEIGHT,
    );
    $items[$base . '/%sky_field_menu_item/sky-field/%sky_field/view'] = array(
      'title' => 'Sky fields',
      'load arguments' => array($type, $field_object_shift + 1),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => SKY_FIELD_MENU_WEIGHT,
    );
    $items[$base . '/%sky_field_menu_item/sky-field/%sky_field/edit'] = array(
      'title callback' => 'sky_field_menu_title_callback',
      'title arguments' => array($field_object_shift + 3, 'edit'),
      'load arguments' => array($type, 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        'sky_field_sky_field_edit_form',
        $field_object_shift + 1,
        $field_object_shift + 3,
      ),
      'access callback' => 'sky_field_entity_access',
      'access arguments' => array($type, $field_object_shift + 1, 'update'),
      'type' => MENU_LOCAL_TASK,
      'file' => 'includes/sky_field.form.sky_field.inc',
      'weight' => SKY_FIELD_MENU_WEIGHT + 1,
    );
    $items[$base . '/%sky_field_menu_item/sky-field/%sky_field/delete'] = array(
      'title callback' => 'sky_field_menu_title_callback',
      'title arguments' => array($field_object_shift + 3, 'delete'),
      'load arguments' => array($type, $field_object_shift + 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        'sky_field_sky_field_delete_form',
        $type,
        $field_object_shift + 1,
        $field_object_shift + 3,
      ),
      'access callback' => 'sky_field_entity_access',
      'access arguments' => array($type, $field_object_shift + 1, 'update'),
      'type' => MENU_LOCAL_TASK,
      'file' => 'includes/sky_field.form.sky_field.inc',
      'weight' => SKY_FIELD_MENU_WEIGHT + 2,
    );
  }
  return $items;
}

/**
 * Define menu item title.
 */
function sky_field_menu_title_callback($field, $op) {
  if ($op == 'edit') {
    return t('Edit "!title" sky field', array('!title' => $field['title']));
  }
  else {
    return t('Delete "!title" sky field', array('!title' => $field['title']));
  }
}

/**
 * Implements hook_admin_paths().
 */
function sky_field_admin_paths() {
  $paths = array(
    '*/sky-field' => TRUE,
    '*/sky-field/*' => TRUE,
  );
  return $paths;
}

/**
 * Menu node loader callback.
 */
function sky_field_menu_item_load($entity_id, $entity_type) {
  if (!$entity_type || !$entity_id) {
    return FALSE;
  }
  if (!sky_field_is_sky_field_entity_type($entity_type)) {
    return FALSE;
  }
  $entity_array = entity_load($entity_type, array($entity_id));
  $entity = reset($entity_array);
  if ($entity_type == 'node') {
    if (!sky_field_is_sky_field_node_type($entity->type)) {
      return FALSE;
    }
  }
  if ($entity) {
    return $entity;
  }
  else {
    return FALSE;
  }
}

/**
 * Menu sky_field loader callback.
 */
function sky_field_load($fid) {
  if ($fid) {
    return sky_field_load_sky_field($fid);
  }
  else {
    return FALSE;
  }
}

/**
 * Find out, if user has access to entity fields.
 */
function sky_field_entity_access($entity_type, $entity, $op, $account = NULL) {
  // If user has Admin sky fields permission, he has full access to sky fields.
  if (user_access('admin sky fields')) {
    return TRUE;
  }
  if (empty($account)) {
    $account = $GLOBALS['user'];
  }
  // Check if any modules implement sky field access customization.
  $access = module_invoke_all('sky_field_access', $entity_type, $entity, $op, $account);
  if (in_array(SKY_FIELD_ACCESS_DENY, $access, TRUE)) {
    return FALSE;
  }
  elseif (in_array(SKY_FIELD_ACCESS_ALLOW, $access, TRUE)) {
    return TRUE;
  }
  // Check access using standard entities access callbacks.
  if ($op == 'view') {
    return user_access('view sky fields');
  }
  if ($op == 'update') {
    if (user_access('manage sky fields')) {
      if ($entity_type == 'node') {
        return node_access('update', $entity);
      }
      elseif ($entity_type == 'user') {
        return user_access('update', $entity);
      }
    }
  }

  return FALSE;
}

/**
 * Implements hook_theme().
 */
function sky_field_theme() {
  $items = array();
  $items['sky_field_field'] = array(
    'render element' => 'element',
    'template' => 'theme/tpl/sky_field_field',
  );
  $items['sky_field_fields'] = array(
    'render element' => 'element',
    'template' => 'theme/tpl/sky_field_fields',
  );
  $items['sky_field_sky_field_page_form_fields'] = array(
    'render element' => 'container',
    'file' => 'theme/sky_field.template.inc',
  );
  return $items;
}

/**
 * Implements hook_field_extra_fields().
 */
function sky_field_field_extra_fields() {
  $items = array();
  $entities = sky_field_get_active_entity();
  foreach ($entities as $entity_type) {
    if (empty($entity_type)) {
      continue;
    }
    $options = array($entity_type);
    if ($entity_type == 'node') {
      $options = variable_get('sky_field_node_types', array());
    }
    foreach ($options as $option) {
      if (empty($option)) {
        continue;
      }
      $items[$entity_type][$option] = array(
        'display' => array(
          'sky_fields' => array(
            'label' => t('Sky fields'),
            'description' => t('Sky fields'),
            'visible' => TRUE,
            'weight' => 0,
          ),
        ),
        'form' => array(
          'sky_fields' => array(
            'label' => t('Sky fields'),
            'description' => t('Sky fields'),
            'visible' => TRUE,
            'weight' => 0,
          ),
        ),
      );
    }
  }
  return $items;
}

/**
 * Implements hook_field_attach_form().
 */
function sky_field_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
  if (!sky_field_is_sky_field_entity($entity_type, $entity)) {
    return;
  }
  form_load_include($form_state, 'inc', 'sky_field', 'includes/sky_field.form');
  sky_field_form_edit($entity_type, $entity, $form, $form_state);
}

/**
 * Implements hook_field_attach_submit().
 */
function sky_field_field_attach_submit($entity_type, $entity, $form, &$form_state) {
  if (!sky_field_is_sky_field_entity($entity_type, $entity)) {
    return;
  }
  if (empty($form_state['values']['sky_fields'])) {
    return;
  }
  foreach ($form_state['values']['sky_fields'] as $fid => $value) {
    $field = array(
      'id' => $fid,
      'value' => $value,
    );
    sky_field_update_sky_field($field);
  }
}

/**
 * Implements hook_entity_view().
 */
function sky_field_entity_view($entity, $type, $view_mode, $langcode) {
  if (!sky_field_is_sky_field_entity($type, $entity)) {
    return;
  }
  // Check sky field view access.
  if (!sky_field_entity_access($type, $entity, 'view')) {
    return;
  }
  require_once 'includes/pages/sky_field.pages.inc';
  // Adding sky_fields to content.
  sky_field_entity_view_add_sky_fields($type, $entity);
}

/**
 * Implements hook_entity_update().
 *
 * Allows to change sky fields param in code.
 * Use sky_field_get_sky_fields and sky_fields_set_sky_fields functions.
 */
function sky_field_entity_update($entity, $type) {
  if (!sky_field_is_sky_field_entity($type, $entity)) {
    return;
  }
  if (!sky_field_is_sky_fields_changed($entity)) {
    return;
  }
  $sky_fields = sky_field_get_sky_fields($type, $entity);
  foreach ($sky_fields as $field) {
    sky_field_update_sky_field($field);
  }
}

/**
 * Implements hook_entity_delete().
 */
function sky_field_entity_delete($entity, $type) {
  $sky_fields = sky_field_get_sky_fields($type, $entity);
  if ($sky_fields) {
    sky_field_delete_entity_sky_fields($type, $entity);
  }
}

/**
 * Implements hook_taxonomy_term_delete().
 */
function sky_field_taxonomy_term_delete($term) {
  // Delete taxonomy sky field values if term was deleted.
  $query_fields = array(
    'value' => $term->tid,
    'type' => 'taxonomy',
  );
  $sky_fields = sky_field_db_field_select($query_fields);
  foreach ($sky_fields as $item) {
    $item['value'] = NULL;
    sky_field_update_sky_field($item);
  }
}

/**
 * Implements hook_preprocess_THEME().
 */
function sky_field_preprocess_sky_field_field(&$vars) {
  // Default element.
  $element = $vars['element'];
  // Class.
  $class = array();
  $class[] = $element['#sky_field']['show_title'] ? 'with-title' : 'without-title';
  $class[] = 'type-' . $element['#sky_field']['type'];
  $vars['class'] = implode(' ', $class);
  // Show title.
  $vars['show_title'] = $element['#sky_field']['show_title'];
  $vars['title'] = $element['#sky_field']['title'];
  $vars['value'] = $element['#sky_field']['value'];
}

/**
 * Implements hook_preprocess_THEME().
 */
function sky_field_preprocess_sky_field_fields(&$vars) {
  $element = $vars['element'];
  $vars['children'] = $element['#children'];
}
